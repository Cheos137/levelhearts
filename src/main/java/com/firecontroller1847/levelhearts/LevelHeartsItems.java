package com.firecontroller1847.levelhearts;

import com.firecontroller1847.levelhearts.items.ItemHeartContainer;
import com.firecontroller1847.levelhearts.items.ItemHeartPiece;
import net.minecraft.world.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.IForgeRegistry;

@EventBusSubscriber(modid = LevelHearts.MOD_ID, bus = Bus.MOD)
public class LevelHeartsItems {

    public static Item heartPiece;
    public static Item heartContainer;

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event) {
        // Construct
        heartPiece =        new ItemHeartPiece().setRegistryName(LevelHearts.MOD_ID, "heart_piece");
        heartContainer =    new ItemHeartContainer().setRegistryName(LevelHearts.MOD_ID, "heart_container");

        // Register
        IForgeRegistry<Item> registry = event.getRegistry();
        registry.register(heartPiece);
        registry.register(heartContainer);
    }

}
