package com.firecontroller1847.levelhearts.gui;

import com.firecontroller1847.levelhearts.Config;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.Util;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.Mth;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.gui.ForgeIngameGui;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.Random;

/**
 * This class is messed up. The forge class is messed up. All GUI stuff is a mess.
 * Any features for this class need to be implemented manually.
 * See {@link ForgeIngameGui} and {@link net.minecraft.client.gui.Gui} for a code comparison.
 * I use my own implementation based on previous Minecraft versions.
 */
public class IngameGui extends GuiComponent {

    private final Random rand = new Random();
    private Minecraft minecraft;

    // Health stuff
    long healthUpdateCounter;
    int playerHealth;
    int lastPlayerHealth;
    long lastSystemTime;

    public IngameGui() {
        this.minecraft = Minecraft.getInstance();
    }

    @SubscribeEvent
    public void onRenderGameOverlay(RenderGameOverlayEvent.PreLayer event) {
        // Disable all HUD modifications if this option is disabled
        if (!Config.customHud.get()) {
            return;
        }

        if (event.getOverlay() == ForgeIngameGui.ARMOR_LEVEL_ELEMENT) {
            event.setCanceled(true);
            redrawArmor(event);
        } else if (event.getOverlay() == ForgeIngameGui.AIR_LEVEL_ELEMENT && !Config.minimalHud.get()) {
            event.setCanceled(true);
            redrawAir(event);
        }
        if (event.getOverlay() == ForgeIngameGui.PLAYER_HEALTH_ELEMENT) {
            if (Config.minimalHud.get()) {
                event.setCanceled(true);
                redrawHealth(event);
            } else if (Config.hideHud.get()) {
                event.setCanceled(true);
            }
        }
    }

    private void redrawArmor(RenderGameOverlayEvent.PreLayer event) {
        if (minecraft.options.hideGui || !shouldDrawSurvivalElements()) {
            return;
        }

        minecraft.getProfiler().push("armor");
        RenderSystem.enableBlend();

        int top = minecraft.getWindow().getGuiScaledHeight() - 49;
        int left = minecraft.getWindow().getGuiScaledWidth() / 2;
        if (Config.minimalHud.get()) {
            left -= 91;
        } else {
            left += 10;
        }

        int armor = minecraft.player.getArmorValue();
        if (armor > 0) {
            for (int i = 0; i < 10; i++) {
                int threshold = i * 2 + 1;
                if (threshold < armor) {
                    this.blit(event.getMatrixStack(), left + i * 8, top, 34, 9, 9, 9);
                } else if (threshold == armor) {
                    this.blit(event.getMatrixStack(), left + i * 8, top, 25, 9, 9, 9);
                } else {
                    this.blit(event.getMatrixStack(), left + i * 8, top, 16, 9, 9, 9);
                }
            }
        }

        RenderSystem.disableBlend();
        minecraft.getProfiler().pop();
    }

    private void redrawAir(RenderGameOverlayEvent.PreLayer event) {
        if (minecraft.options.hideGui || !shouldDrawSurvivalElements()) {
            return;
        }

        minecraft.getProfiler().push("air");
        RenderSystem.enableBlend();

        int air = minecraft.player.getAirSupply();
        int maxAir = minecraft.player.getMaxAirSupply();
        if (minecraft.player.isEyeInFluid(FluidTags.WATER) || air < maxAir) {

            int top = minecraft.getWindow().getGuiScaledHeight() - 49;
            int left = minecraft.getWindow().getGuiScaledWidth() / 2 + 91;
            if (minecraft.player.getArmorValue() > 0) {
                top -= 10;
            }

            int full = Mth.ceil((air - 2) * 10.0D / maxAir);
            int partial = Mth.ceil(air * 10.0D / maxAir) - full;
            for (int i = 0; i < full + partial; i++) {
                if (i < full) {
                    this.blit(event.getMatrixStack(), left - i * 8 - 9, top, 16, 18, 9, 9);
                } else {
                    this.blit(event.getMatrixStack(), left - i * 8 - 9, top, 25, 18, 9, 9);
                }
            }

        }

        RenderSystem.disableBlend();
        minecraft.getProfiler().pop();
    }

    private void redrawHealth(RenderGameOverlayEvent.PreLayer event) {
        if (minecraft.options.hideGui || !shouldDrawSurvivalElements()) {
            return;
        }

        RenderSystem.setShaderTexture(0, Gui.GUI_ICONS_LOCATION);
        minecraft.getProfiler().push("health");
        RenderSystem.enableBlend();

        int top = minecraft.getWindow().getGuiScaledHeight() - 39;
        int left = minecraft.getWindow().getGuiScaledWidth() / 2 - 91;

        // Shortcut player
        Player player = minecraft.player;

        // Get the player's health (rounded)
        int health = Mth.ceil(player.getHealth());

        // Health update counter
        boolean flash = healthUpdateCounter > minecraft.gui.getGuiTicks() && (healthUpdateCounter - minecraft.gui.getGuiTicks()) / 3L % 2L == 1L;

        // Calculate counter
        long time = Util.getMillis();
        if (health < playerHealth && player.invulnerableTime > 0) {
            lastSystemTime = time;
            healthUpdateCounter = minecraft.gui.getGuiTicks() + 20;
        } else if (health > this.playerHealth && player.invulnerableTime > 0) {
            lastSystemTime = time;
            healthUpdateCounter = minecraft.gui.getGuiTicks() + 10;
        }
        if (time - lastSystemTime > 1000L) {
            playerHealth = health;
            lastPlayerHealth = health;
            lastSystemTime = time;
        }

        // Set some variables
        this.playerHealth = health;
        this.rand.setSeed(minecraft.gui.getGuiTicks() * 312871L);

        // Get absorption amount
        int absHealth = Mth.ceil(player.getAbsorptionAmount());
        int absHealthTemp = absHealth;

        // Get amount in full hearts subtract one
        int hearts = Mth.ceil((player.getHealth() + absHealth) / 2.0F) - 1;

        // Get max amount in full hearts subtract one
        int maxHearts = Mth.ceil((player.getMaxHealth() + absHealth) / 2.0F) - 1;

        // Determine how many rows and fetch the top one (-1 for zero index)
        int row = Mth.ceil(((hearts + 1) * 2) / 20.0F) - 1;

        // Determine the maximum amount of rows possible
        int maxRow = Mth.ceil(((maxHearts + 1) * 2) / 20.0F) - 1;

        // Get minimum heart range for row
        int rowMin = row * 10;

        // Get the maximum amount of hearts for this row
        int rowMax = rowMin + 10;

        // Remove extra hearts that would have exceeded the player's maximum heart count on the last row only
        int rowPreferred = row == maxRow ? rowMax - (10 - (maxHearts + 1 - rowMin)) : rowMax;

        for (int i = rowMin; i < rowPreferred; i++) {
            int textureX = 16;
            if (player.hasEffect(MobEffects.POISON)) {
                textureX += 36;
            } else if (player.hasEffect(MobEffects.WITHER)) {
                textureX += 72;
            }

            int regen = -1;
            if (player.hasEffect(MobEffects.REGENERATION)) {
                regen = minecraft.gui.getGuiTicks() % Mth.ceil(player.getMaxHealth() + 5.0F);
            }
            int x = left + (i * 8) - ((8 * 10) * row);
            int y = top;

            // Check if hearts is less than 40% of default health (min 1)
            if (hearts <= Math.max((Config.defHealth.get() / 2.0) * 0.4, 1)) {
                y += this.rand.nextInt(2);
            }

            if (absHealthTemp <= 0 && i == regen) {
                y -= 2;
            }

            // Draw Empty Hearts
            this.blit(event.getMatrixStack(), x, y, 16 + (flash ? 9 : 0), 0, 9, 9);

            // Draw Flag Hearts?
            if (flash) {
                if (i * 2 + 1 < this.lastPlayerHealth) {
                    this.blit(event.getMatrixStack(), x, y, textureX + 54, 0, 9, 9);
                }
                if (i * 2 + 1 == this.lastPlayerHealth) {
                    this.blit(event.getMatrixStack(), x, y, textureX + 63, 0, 9, 9);
                }
            }

            // Draw Filled In Hearts
            if (i * 2 + 1 < health) {
                this.blit(event.getMatrixStack(), x, y, textureX + 36, 0, 9, 9);
            } else if (i * 2 + 1 == health) {
                this.blit(event.getMatrixStack(), x, y, textureX + 45, 0, 9, 9);
            } else if (i * 2 + 1 > health && absHealthTemp > 0) {
                if (absHealthTemp == absHealth && absHealth % 2 == 1) {
                    this.blit(event.getMatrixStack(), x, y, textureX + 153, 0, 9, 9);
                    absHealthTemp--;
                } else {
                    this.blit(event.getMatrixStack(), x, y, textureX + 144, 0, 9, 9);
                    absHealthTemp -= 2;
                }
            }
        }

        // Draw MinimalHUD Number
        String text = Integer.toString(row + 1);

        // Move left positioning for more positions in the number
        if (row >= 9)
            left -= 6;
        if (row >= 99)
            left -= 6;
        if (row >= 999)
            left -= 6;
        if (row >= 9999) {
            text = "9999+";
            left -= 6;
        }

        // Dropshadow
        minecraft.font.draw(event.getMatrixStack(), text, left - 6, top + 1, 0x000000);
        minecraft.font.draw(event.getMatrixStack(), text, left - 8, top + 1, 0x000000);
        minecraft.font.draw(event.getMatrixStack(), text, left - 7, top + 2, 0x000000);
        minecraft.font.draw(event.getMatrixStack(), text, left - 7, top, 0x000000);

        // The Number
        minecraft.font.draw(event.getMatrixStack(), text, left - 7, top + 1, 0xF00000);

        RenderSystem.disableBlend();
        minecraft.getProfiler().pop();
        RenderSystem.setShaderTexture(0, Gui.GUI_ICONS_LOCATION);
    }

    /**
     * This is literally a duplicate of {@link ForgeIngameGui#shouldDrawSurvivalElements()},
     * but I'm dumb and can't figure out how to get an instance of that so I just copied it instead. lol
     */
    private boolean shouldDrawSurvivalElements() {
        return minecraft.gameMode.canHurtPlayer() && minecraft.getCameraEntity() instanceof Player;
    }

}
