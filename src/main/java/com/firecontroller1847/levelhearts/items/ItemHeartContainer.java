package com.firecontroller1847.levelhearts.items;

import com.firecontroller1847.levelhearts.Config;
import com.firecontroller1847.levelhearts.LevelHearts;
import com.firecontroller1847.levelhearts.LevelHeartsItems;
import com.firecontroller1847.levelhearts.capabilities.IMoreHealth;
import com.firecontroller1847.levelhearts.capabilities.MoreHealth;
import net.minecraft.Util;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

import static com.firecontroller1847.levelhearts.LevelHearts.debug;

public class ItemHeartContainer extends Item {

    public ItemHeartContainer() {
        super(new Properties().stacksTo(1).tab(CreativeModeTab.TAB_MISC));
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level level, Player player, InteractionHand hand) {
        // Setup return results
        ItemStack stack = player.getItemInHand(hand);
        InteractionResultHolder<ItemStack> result = new InteractionResultHolder<>(InteractionResult.PASS, stack);

        // Ensure server-side only & the player's not in creative or spectator
        if (level.isClientSide || player.getAbilities().invulnerable) {
            return result;
        }

        // Ensure item is a heart container
        if (!stack.getItem().equals(LevelHeartsItems.heartContainer)) {
            return result;
        }

        // Debug
        debug("HeartContainerRightClick{Event}");

        // If disabled don't do anything
        if (!Config.enableItems.get()) {
            player.sendMessage(new TranslatableComponent("text." + LevelHearts.MOD_ID + ".disabled"), Util.NIL_UUID);
            return result;
        }

        // Get capability
        IMoreHealth cap = MoreHealth.getFromPlayer(player);

        // Check if player is at max health
        int max = Config.maxHealth.get();
        if (max <= 0 || player.getMaxHealth() < max) {
            // If we're using absorption hearts, just add to the absorption level
            // Otherwise, check against max containers
            if (Config.itemAbsorption.get()) {
                player.setAbsorptionAmount(player.getAbsorptionAmount() + 2.0f);
                player.sendMessage(new TranslatableComponent("text." + LevelHearts.MOD_ID + ".heartadded"), Util.NIL_UUID);
                debug("HeartContainerRightClick{Event}: Added new heart.");

                // Play levelup noise
                if (Config.playSoundOnHeartGain.get()) {
                    player.level.playSound(null, player.getX(), player.getY(), player.getZ(), SoundEvents.PLAYER_LEVELUP, player.getSoundSource(), 0.80F, 1.0F);
                }
            } else if (cap.getHeartContainers() + 1 <= 127) {
                cap.addHeartContainer();
                MoreHealth.updateClient((ServerPlayer) player, cap);
                LevelHearts.applyHealthModifier(player, cap.getTrueModifier());
                player.sendMessage(new TranslatableComponent("text." + LevelHearts.MOD_ID + ".heartadded"), Util.NIL_UUID);
                debug("HeartContainerRightClick{Event}: Added new heart.");

                // Play levelup noise
                if (Config.playSoundOnHeartGain.get()) {
                    player.level.playSound(null, player.getX(), player.getY(), player.getZ(), SoundEvents.PLAYER_LEVELUP, player.getSoundSource(), 0.80F, 1.0F);
                }
            } else {
                debug("HeartContainerRightClick{Event}: Player is at max containers.");
            }
        } else {
            debug("HeartContainerRightClick{Event}: Player is at max health.");
        }

        // Actually heal the player
        int healAmount = Config.healAmount.get();
        if (healAmount <= 0) {
            player.setHealth(player.getMaxHealth());
            player.sendMessage(new TranslatableComponent("text." + LevelHearts.MOD_ID + ".replenished"), Util.NIL_UUID);
            debug("HeartContainerRightClick{Event}: Replenished health.");
        } else {
            player.setHealth(Math.min(player.getHealth() + healAmount * 2, player.getMaxHealth()));
            player.sendMessage(new TranslatableComponent("text." + LevelHearts.MOD_ID + ".healed", healAmount), Util.NIL_UUID);
            debug("HeartContainerRightClick{Event}: Healed the player " + healAmount + " health.");
        }

        // Remove item and mark as success
        stack.setCount(stack.getCount() - 1);
        return new InteractionResultHolder<>(InteractionResult.SUCCESS, stack);
    }

}
