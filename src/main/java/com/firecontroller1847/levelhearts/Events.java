package com.firecontroller1847.levelhearts;

import com.firecontroller1847.levelhearts.capabilities.IMoreHealth;
import com.firecontroller1847.levelhearts.capabilities.MoreHealth;
import com.firecontroller1847.levelhearts.capabilities.MoreHealthProvider;
import com.firecontroller1847.levelhearts.commands.LevelHeartsCommand;
import com.mojang.brigadier.CommandDispatcher;
import net.minecraft.Util;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.RegisterCommandsEvent;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingExperienceDropEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.PlayerChangedDimensionEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.PlayerRespawnEvent;
import net.minecraftforge.event.entity.player.PlayerXpEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.util.ObfuscationReflectionHelper;

import static com.firecontroller1847.levelhearts.LevelHearts.debug;

@EventBusSubscriber(modid = LevelHearts.MOD_ID, bus = Bus.FORGE)
public class Events {

    @SubscribeEvent
    public static void onPlayerLogin(PlayerLoggedInEvent event) {
        // Ensure server-side only
        if (event.getPlayer().getCommandSenderWorld().isClientSide) {
            return;
        }
        debug("PlayerLoggedIn{Event}");

        // Fetch Capability
        Player player = event.getPlayer();
        IMoreHealth cap = MoreHealth.getFromPlayer(player);

        // Apply Health Modifier
        LevelHearts.applyHealthModifier(player, cap.getTrueModifier());

        // Initial Setup
        if (cap.getVersion() == 1) {
            debug("PlayerLoggedIn{Event}: New player! Initiating data.");

            // Search for Old Data
            debug("PlayerLoggedIn{Event}: Searching for v1 data.");
            CompoundTag data = player.getPersistentData();
            if (data.contains("levelHearts")) {

                // Miragte v1 Data
                debug("PlayerLoggedIn{Event}: Data found. Migrating!");
                CompoundTag tag = data.getCompound("levelHearts");
                cap.setModifier((float) tag.getDouble("modifier"));
                cap.setRampPosition(tag.getByte("levelRampPosition"));
                cap.setHeartContainers(tag.getByte("heartContainers"));

                // Apply new health modifier
                LevelHearts.applyHealthModifier(player, cap.getTrueModifier());

                // Remove Old Data
                data.remove("levelHearts");
            } else {
                debug("PlayerLoggedIn{Event}: No v1 data found.");
            }

            // Set health to max & up version
            player.setHealth(player.getMaxHealth());
            cap.setVersion((byte) 2);
        }
    }

    @SubscribeEvent
    public static void onAttachCapabilities(AttachCapabilitiesEvent<Entity> event) {
        if (event.getObject() instanceof Player) {
            debug("AttachCapabilities{Event}");
            event.addCapability(new ResourceLocation(LevelHearts.MOD_ID, "morehealth"), new MoreHealthProvider());
        }
    }

    @SubscribeEvent
    public static void onPlayerClone(PlayerEvent.Clone event) {
        debug("PlayerClone{Event}");

        // Fetch & Copy Capability
        Player playerOld = event.getOriginal();
        playerOld.reviveCaps();
        Player playerNew = event.getPlayer();
        IMoreHealth capOld = MoreHealth.getFromPlayer(playerOld);
        IMoreHealth capNew = MoreHealth.getFromPlayer(playerNew);
        capNew.copy(capOld);
        playerOld.invalidateCaps();
        
        // Copy Health on Dimension Change
        if (!event.isWasDeath()) {
        	debug("PlayerClone{Event}: Cloning from dimension change, re-applying health modifier.");
        	LevelHearts.applyHealthModifier(playerNew, capNew.getTrueModifier());
        	playerNew.setHealth(playerOld.getHealth());
        }
    }

    @SubscribeEvent
    public static void onPlayerRespawn(PlayerRespawnEvent event) {
        // Ensure server-side only
        if (event.getPlayer().getCommandSenderWorld().isClientSide) {
            return;
        }
        debug("PlayerRespawn{Event}");

        // Fetch Capability
        Player player = event.getPlayer();
        IMoreHealth cap = MoreHealth.getFromPlayer(player);

        // Handle "The End"
        if (event.isEndConquered()) {
            debug("PlayerRespawn{Event}: Coming from end, syncing!");
            MoreHealth.updateClient((ServerPlayer) player, cap);
            return;
        }

        // If we're using xp absorption, reset the level ramp since the player lost all their hearts
        if (Config.xpAbsorption.get()) {
            cap.setRampPosition((short) 0);
            MoreHealth.updateClient((ServerPlayer) player, cap);
        }

        // Handle Hardcore Mode
        if (Config.enableHardcore.get()) {
            debug("PlayerRespawn{Event}: Hardcore mode enabled, removing health.");
            
            // Send Message
            player.sendMessage(new TranslatableComponent("text." + LevelHearts.MOD_ID + ".hardcore"), Util.NIL_UUID);
            
            // Reset Capability
            cap.setModifier(MoreHealth.getDefaultModifier());
            cap.setRampPosition((short) 0);
            cap.setHeartContainers((byte) 0);
            MoreHealth.updateClient((ServerPlayer) player, cap);
         
        // Handle Punishing the Player
	    } else if (Config.punishAmount.get() > 0) {
	    	debug("PlayerRespawn{Event}: Punishment enabled, removing health.");

	    	int amount = Config.punishAmount.get() * 2; // The amount of health to remove
            float oldModifier = cap.getModifier(); // The old modifier before having removed the amount
            float newModifier = oldModifier - amount; // The new modifier after having removed the amount

            // If the new modifier is less than the default modifier, set to default & remove the necessary heart containers
            // Otherwise, the newModifier is from experience, so we can just use that
            if (newModifier < MoreHealth.getMinimumModifier()) {
                float healthLeft = (newModifier - MoreHealth.getMinimumModifier()); // The amount of health left to remove after setting to default (should be negative)
                byte containers = cap.getHeartContainers(); // How many heart containers before having removed the health
                byte containersLeft = (byte) Math.max((containers + (healthLeft / 2)), 0); // How many heart containers the player still has after removing the health (min 0)

                // Set Data
                cap.setModifier(MoreHealth.getMinimumModifier());
                cap.setRampPosition((short) 0);
                cap.setHeartContainers(containersLeft);

                // Send the Message
                byte containersLost = (byte) (containers - containersLeft); // Calculate Containers Lost
                if (containersLost > 0) {
                    player.sendMessage(new TranslatableComponent("text." + LevelHearts.MOD_ID + ".punish", containersLost), Util.NIL_UUID);
                }
            } else {
                cap.setModifier(newModifier);
                cap.setRampPosition((short) (newModifier / 2));

                // Send the Message
                player.sendMessage(new TranslatableComponent("text." + LevelHearts.MOD_ID + ".punish", (amount / 2)), Util.NIL_UUID);
            }

            // Notify Client of Capability Changes
            MoreHealth.updateClient((ServerPlayer) player, cap);
	    }

        // Handle force lose xp on death
        if (Config.loseXpOnDeath.get()) {
            player.giveExperienceLevels(-player.experienceLevel - 1);
        }

        // Re-add health modifier and fill health
        LevelHearts.recalcPlayerHealth(player, player.experienceLevel);
        LevelHearts.applyHealthModifier(player, cap.getTrueModifier());
        player.setHealth(player.getMaxHealth());
    }

    @SubscribeEvent
    public static void onLivingDamage(LivingDamageEvent event) {
        // Ensure server-side only
        if (event.getEntity().getCommandSenderWorld().isClientSide) {
            return;
        }
        debug("LivingDamage{Event}");

        // Ensure player only
        Player player;
        if (!(event.getEntity() instanceof Player)) {
            return;
        } else {
            player = (Player) event.getEntity();
        }

        // Check for OHKO
        if (Config.enableOhko.get()) {
            player.setHealth(0);
        }
    }

    @SubscribeEvent
    public static void onLivingDeath(LivingDeathEvent event) {
        // Ensure server-side only
        if (event.getEntity().getCommandSenderWorld().isClientSide) {
            return;
        }
        debug("LivingDeath{Event}");

        // Ensure player only
        Player player;
        if (!(event.getEntity() instanceof Player)) {
            return;
        } else {
            player = (Player) event.getEntity();
        }

        // Check for Keep Experience on Death
        if (Config.loseInvOnDeath.get()) {
            // Essentially, call Player#dropEquipment, but ignoring the KEEP_INVENTORY gamerule
            try {
                ObfuscationReflectionHelper.findMethod(Player.class, "dropEquipment").invoke(player);
                ObfuscationReflectionHelper.findMethod(Player.class, "destroyVanishingCursedItems").invoke(player);
            } catch (Exception e) {
                e.printStackTrace();
            }
            player.getInventory().dropAll();
        }
    }

    @SubscribeEvent
    public static void onPlayerChangedDimension(PlayerChangedDimensionEvent event) {
        // Ensure server-side only
        if (event.getPlayer().getCommandSenderWorld().isClientSide) {
            return;
        }
        debug("PlayerChangedDimension{Event}");

        // Fetch Capability
        Player player = event.getPlayer();
        IMoreHealth cap = MoreHealth.getFromPlayer(player);

        // Re-add health modifier
        LevelHearts.applyHealthModifier(player, cap.getTrueModifier());

        // Synchronize
        cap.synchronise(player);
    }

    @SubscribeEvent
    public static void onPlayerLevelUp(PlayerXpEvent.LevelChange event) {
        // Ensure server-side only
        if (event.getPlayer().getCommandSenderWorld().isClientSide) {
            return;
        }
        debug("PlayerLevelChange{Event}");

        // Fetch player and recalc player's health
        Player player = event.getPlayer();
        LevelHearts.recalcPlayerHealth(player, player.experienceLevel + event.getLevels());
    }

    @SubscribeEvent
    public static void onPlayerPickupXp(PlayerXpEvent.PickupXp event) {
        debug("PlayerPickupXp{Event}");
        event.getOrb().value *= Config.xpMultiplier.get();
    }

    @SubscribeEvent
    public static void onExperienceDrop(LivingExperienceDropEvent event) {
        if (Config.loseXpOnDeath.get() && (event.getEntity() instanceof Player)) {
            debug("ExperienceDrop{Event}");
            Player player = (Player) event.getEntity();

            // See PlayerEntity experience drop code
            int i = player.experienceLevel * 7;
            event.setDroppedExperience(Math.min(i, 100));
        }
    }

    @SubscribeEvent
    public static void onRegisterCommands(RegisterCommandsEvent event) {
        CommandDispatcher<CommandSourceStack> dispatcher = event.getDispatcher();
        LevelHeartsCommand.register(dispatcher);
    }

    /**
     * Test code that enables generating any loot table on the right click of a chest while holding a heart piece.
     * @param event
     */
//     @SubscribeEvent
//     public static void onBlockClicked(PlayerInteractEvent.RightClickBlock event) {
//         if (!event.getWorld().isClientSide) {
//             ServerLevel world = (ServerLevel) event.getWorld();
//             ServerPlayer player = (ServerPlayer) event.getPlayer();
//
//             if (player.getMainHandItem().getItem() instanceof ItemHeartPiece) {
//                 System.out.println("IS HEART PIECE");
//
//                 BlockEntity entity = world.getBlockEntity(event.getPos());
//                 if (entity != null && entity instanceof ChestBlockEntity) {
//                     System.out.println("IS CHEST");
//                     ChestBlockEntity chest = (ChestBlockEntity) entity;
//                     chest.clearContent();
//
//                     String name = "village_weaponsmith";
//                     LootTable table = world.getServer().getLootTables().get(new ResourceLocation("minecraft:chests/village/" + name));
//                     if (table != null) {
//                         LootContext ctx = new LootContext.Builder(world).withLuck(player.getLuck()).create(new LootContextParamSet.Builder().build());
//                         table.fill(chest, ctx);
//                     }
//                 }
//             }
//         }
//     }

}
